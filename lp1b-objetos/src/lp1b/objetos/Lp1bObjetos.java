/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lp1b.objetos;

/**
 *
 * @author edgar
 */
public class Lp1bObjetos {
    public static int x = 10;
    
    public static void primermetodo(){
        System.out.println("Primer metodo");
    }

    public static void main(String[] args) {
         // Clases y objetos
        /*
        System.out.println(Lp1bObjetos.x);
        Lp1bObjetos.x = 20;
        System.out.println(Lp1bObjetos.x);
        primermetodo();
        Lp1bObjetos.primermetodo();    
        */
        //int[] arreglo = new int[2];
        
        /*
        operaciones calculadora = new operaciones();
        calculadora.setResultado(0);
        calculadora.suma(5);
        System.out.println(calculadora.toString());
        calculadora.suma(15);
        System.out.println(calculadora.toString());
        calculadora.suma(10);
        System.out.println(calculadora.toString());
        calculadora.suma(30);
        System.out.println(calculadora.toString());
        
        operaciones cientifica = new operaciones(1);
        System.out.println(cientifica.toString());
       */
        
        animales perro = new animales();
        
        perro.setTipo("Mamifero");
        perro.setRaza("Perro");
        perro.setColor("Negro");
        perro.setEdad(1);
        perro.setAltura(0.60);
        perro.setPeso(5.5);
        perro.setSexo('M');
        
        
        System.out.println(perro.toString());
        perro.comer();
        
        animales ave = new animales("Ave","Gallina", "Roja", 1, .40, 3.0, 'F');
        System.out.println(ave.toString());
        ave.levantarse();
        ave.setPeso(3.5);
        System.out.println(ave.toString());
        ave.dormir();
        
        
    }
    
}
