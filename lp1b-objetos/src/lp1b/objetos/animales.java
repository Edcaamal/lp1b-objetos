/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp1b.objetos;

/**
 *
 * @author edgar
 */
public class animales {
    private String tipo;
    private String raza;
    private String color; 
    private int edad;
    private double altura;
    private double peso;
    private char sexo;

    // Contructor Vacio
    public animales() {
    }
    
    // Contruye y recibe atributos y los asigna
    public animales(String tipo, String raza, String color, int edad, double altura, double peso, char sexo) {
        this.tipo = tipo;
        this.raza = raza;
        this.color = color;
        this.edad = edad;
        this.altura = altura;
        this.peso = peso;
        this.sexo = sexo;
    }



    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public void comer() {
        System.out.println("El Animal esta comiendo ... ");
    }
    
    public void levantarse() {
        System.out.println("El Animal se esta levantamdo ... ");
    }
    public void dormir() {
        System.out.println("El Animal se va a dormir ... ");
    }
    
    
    @Override
    public String toString() {
        return "animales{" + "tipo=" + tipo + ", raza=" + raza + ", color=" + color + ", edad=" + edad + ", altura=" + altura + ", peso=" + peso + ", sexo=" + sexo + '}';
    }

    
}
